package main

import (
	"path/filepath"
	"strings"
	"testing"
)

var configFileText = `
_data\top1\*
_data\top2\**
_data\top3\
_data\top4
_data/top5
`

func Test_loadDirs(t *testing.T) {
	r := strings.NewReader(configFileText)
	list := loadDirs(r)
	expected := []string{
		`_data\top1\`, `_data\top1\inside1\`, `_data\top1\inside2\`,
		`_data\top2\`, `_data\top2\inside1\`, `_data\top2\inside1\insideinside\`,
		`_data\top3\`,
		`_data\top4\`,
		`_data\top5\`,
	}
	if filepath.Separator == '/' {
		for i := range expected {
			expected[i] = strings.Replace(expected[i], `\`, `/`, -1)
		}
	}
	if !isStringSliceEqual(list, expected) {
		t.Errorf("Expected: %v\nActual: %v", expected, list)
	}
}

func isStringSliceEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
