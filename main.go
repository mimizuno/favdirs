package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/monochromegane/go-home"
)

func main() {
	homeDir := home.Dir()

	name := filepath.Join(homeDir, getConfigFileName())

	f, err := os.Open(name)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	list := loadDirs(f)
	for _, l := range list {
		fmt.Println(l)
	}
}

func getConfigFileName() string {
	return ".favdirs"
}

type dirlist []string

func loadDirs(r io.Reader) []string {
	list := make([]string, 0)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		t := scanner.Text()
		if t == "" {
			continue
		}
		if strings.HasPrefix(t, `\\`) {
			list = append(list, t)
			continue
		}
		if filepath.Separator == '/' {
			t = strings.Replace(t, `\`, `/`, -1)
		} else {
			t = strings.Replace(t, `/`, `\`, -1)
		}
		t = filepath.Clean(t)
		dir, base := splitpath(t)
		dir = ensureDriveRootSuffix(dir)
		if base == "*" {
			list = addPath(list, dir)
			list = addPathChildlen(list, dir, 1)
		} else if base == "**" {
			list = addPath(list, dir)
			list = addPathChildlen(list, dir, -1)
		} else {
			list = addPath(list, filepath.Join(dir, base))
		}
	}
	return removeDuplicate(list)
}

func addPath(l []string, p string) []string {
	stat, err := os.Stat(p)
	if err == nil {
		if stat.IsDir() {
			return append(l, ensureDirectorySuffix(p))
		}
		return append(l, p)
	}
	return l
}

func addPathChildlen(l []string, p string, level int) []string {
	if level == 0 {
		return l
	}
	if !isExist(p) {
		return l
	}
	d, err := os.Open(p)
	if err != nil {
		return l
	}
	defer d.Close()
	if fi, err := d.Stat(); err != nil || !fi.IsDir() {
		return l
	}
	fis, err := d.Readdir(-1)
	if err != nil {
		return l
	}
	for _, fi := range fis {
		if fi.IsDir() {
			n := filepath.Join(p, fi.Name())
			l = append(l, ensureDirectorySuffix(n))
			l = addPathChildlen(l, n, level-1)
		}
	}
	return l
}

func isExist(p string) bool {
	_, err := os.Stat(p)
	return err == nil
}

func splitpath(p string) (string, string) {
	pos := strings.LastIndex(p, string([]rune{filepath.Separator}))
	if pos < 0 {
		return p, ""
	}
	if pos == len(p)-1 {
		return p[:pos], ""
	}
	return p[:pos], p[pos+1:]
}

func removeDuplicate(l []string) []string {
	result := make([]string, 0, len(l))
	dup := make(map[string]bool)
	for _, p := range l {
		if !dup[p] {
			dup[p] = true
			result = append(result, p)
		}
	}
	return result
}

func ensureDriveRootSuffix(d string) string {
	if runtime.GOOS != "windows" {
		return d
	}
	if len(d) == 2 && d[1] == ':' {
		return d + string(filepath.Separator)
	}
	return d
}

func ensureDirectorySuffix(p string) string {
	if p[len(p)-1] != filepath.Separator {
		return p + string(filepath.Separator)
	}
	return p
}
