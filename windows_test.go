// +build windows

package main

import (
	"strings"
	"testing"
)

func Test_loadDirsWindowsPath(t *testing.T) {
	r := strings.NewReader(`C:\Windows
C:\Windows\System32
C:\`)
	expected := []string{
		`C:\Windows\`, `C:\Windows\System32\`, `C:\`,
	}
	list := loadDirs(r)

	if !isStringSliceEqual(list, expected) {
		t.Errorf("Expected: %v\nActual: %v", expected, list)
	}
}
